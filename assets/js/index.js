let main = () => {
  let bai_1 = () => {
    const MAX_ROW = 10;
    let i, j, k;
    let table_html = "";
    let row_html = "";
    for (i = 0; i < MAX_ROW; i++) {
      k = i * 10 + 1;
      row_html += "<tr>";
      for (j = k; j <= (i + 1) * 10; j++) {
        row_html += `<td>${j}</td>`;
      }
      row_html += "</tr>";
    }
    table_html = `<table class="table">${row_html}</table>`;
    document.querySelectorAll(".txt-result-bai-1")[0].innerHTML = table_html;
  };

  let bai_2 = (totalInput = []) => {
    let flag,
      i,
      // finalResult = [],
      output = "";
    totalInput.forEach((input) => {
      flag = 1;
      if (input < 2) {
        flag = 0;
      } else {
        for (i = 2; i <= Math.sqrt(input); i++) {
          if (!(input % i)) {
            flag = 0;
            break;
          }
        }
      }
      if (flag) {
        output += `${input} `;
      }
    });

    document.querySelectorAll(".txt-result-bai-2")[0].innerHTML = output;
  };

  let bai_3 = (input) => {
    let sum_1 = 0,
      sum = 0,
      i;
    for (i = 2; i <= input; i++) {
      sum_1 += i;
    }
    sum = sum_1 + 2 * input;
    output = `S=(2+3+4...+n)+2n = : ${sum}`;
    document.querySelectorAll(".txt-result-bai-3")[0].innerHTML = output;
  };

  let bai_4 = () => {
    let n =
      parseInt(document.querySelectorAll(".bai-tap-4 .inputNumber")[0].value) ||
      0;

    let i,
      result = [];
    for (i = 1; i <= n; i++) {
      if (!(n % i)) {
        result.push(i);
      }
    }
    output = `Danh sách ước số của ${n}: ${result.join(", ")}`;
    document.querySelectorAll(".txt-result-bai-4")[0].innerHTML = output;
  };

  let bai_5 = () => {
    let n = document.querySelectorAll(".bai-tap-5 .inputNumber")[0].value;
    let result = n.split("").reverse().join("");
    output = `Đảo ngược của ${n} là : ${result}`;
    document.querySelectorAll(".txt-result-bai-5")[0].innerHTML = output;
  };

  let bai_6 = () => {
    let i,
      sum = 0,
      n;
    for (i = 1; i <= 100; i++) {
      sum += i;
      if (sum > 100) {
        break;
      }
    }
    output = `X lớn nhất để: 1+2+3+...+x <= 100 là: ${i}`;
    document.querySelectorAll(".txt-result-bai-6")[0].innerHTML = output;
  };
  let bai_7 = () => {
    let i,
      output = "";
    let n =
      parseInt(document.querySelectorAll(".bai-tap-7 .inputNumber")[0].value) ||
      0;
    if (n > 0) {
      for (i = 0; i <= 10; i++) {
        output += `<span>${n} * ${i} = ${n * i}</span><br>`;
      }
    }
    document.querySelectorAll(".txt-result-bai-7")[0].innerHTML = output;
  };

  let bai_8 = () => {
    let i,
      cards = [
        "4K",
        "KH",
        "5C",
        "KA",
        "QH",
        "KD",
        "2H",
        "10S",
        "AS",
        "7H",
        "9K",
        "10D",
      ];
    let players = [[], [], [], []];

    for (i = 0; i < cards.length; i += 4) {
      players[0].push(cards[i]);
    }
    console.log(players[0]);
    for (i = 1; i < cards.length; i += 4) {
      players[1].push(cards[i]);
    }
    console.log(players[1]);
    for (i = 2; i < cards.length; i += 4) {
      players[2].push(cards[i]);
    }
    console.log(players[2]);
    for (i = 3; i < cards.length; i += 4) {
      players[3].push(cards[i]);
    }
    console.log(players[3]);

    output = `
      <div>Player 1 : ${players[0].join(", ")}</div>
      <div>Player 2 : ${players[1].join(", ")}</div>
      <div>Player 3 : ${players[2].join(", ")}</div>
      <div>Player 4 : ${players[3].join(", ")}</div>
      
    `;
    document.querySelectorAll(".txt-result-bai-8")[0].innerHTML = output;
  };

  let bai_9 = () => {
    let m =
        parseInt(
          document.querySelectorAll(".bai-tap-9 .inputNumberM")[0].value
        ) || 0,
      n =
        parseInt(
          document.querySelectorAll(".bai-tap-9 .inputNumberN")[0].value
        ) || 0,
      output = "";
    try {
      if (m == 0 || n == 0) {
        throw "Please do not enter 0 for any input(s)";
      }
      if (n % 2 || n > 4 * m || n < 2 * m) {
        throw "Please input correct number for n and m";
      } else {
        let numb_chicken = (4 * m - n) / 2;
        let numb_dog = m - numb_chicken;
        output = `
          <div>Số chó: ${numb_dog}</div>
          <div>Số gà: ${numb_chicken}</div>
        `;
      }
    } catch (error) {
      console.error(`Error ${error}`);
      output = error;
    }
    document.querySelectorAll(".txt-result-bai-9")[0].innerHTML = output;
  };

  let bai_10 = () => {
    let hour =
        parseInt(
          document.querySelectorAll(".bai-tap-10 .inputNumberH")[0].value
        ) || 0,
      min =
        parseInt(
          document.querySelectorAll(".bai-tap-10 .inputNumberM")[0].value
        ) || 0,
      deg = 0,
      degHourToMinute = 0,
      degMinuteToHour = 0,
      output = "";
    const DEG_HOUR_HAND_PER_HOUR = 30,
      DEG_MIN_HAND_PER_HOUR = 6,
      DEG_HOUR_HAND_PER_MIN = 0.5;
    try {
      if (hour < 0 || min < 0) {
        throw "Please input positive number for hour and min";
      }
      if (hour === 0 && min === 0) {
        deg = 0;
      } else {
        deg = Math.abs(
          DEG_HOUR_HAND_PER_HOUR * hour +
            DEG_HOUR_HAND_PER_MIN * min -
            DEG_MIN_HAND_PER_HOUR * min
        );
      }
      if (deg > 180) {
        degHourToMinute = deg;
        degMinuteToHour = 360 - degHourToMinute;
      } else {
        degMinuteToHour = deg;
        degHourToMinute = 360 - degMinuteToHour;
      }
      output = `
      <p>Lúc ${hour} giờ ${min} phút kim giờ và kim phút sẽ tạo thành: </p>
      <p>Góc giữa kim giờ và kim phút: ${degHourToMinute} độ</p>
      <p>Góc giữa kim phút và kim giờ: ${degMinuteToHour} độ</p>
      `;
    } catch (error) {
      console.error(`Error ${error}`);
      output = `<p class="bg-danger text-white">${error}</p>`;
    }
    document.querySelectorAll(".txt-result-bai-10")[0].innerHTML = output;
  };

  document
    .querySelectorAll(".btn-calc-bai-1")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_1();
    });

  // giai bai 2
  let bai2Input = [];
  document
    .querySelectorAll(".bai-tap-2 #btn-add")[0]
    .addEventListener("click", () => {
      let input = document.querySelectorAll(".bai-tap-2 #inputNumber")[0].value;
      bai2Input.push(parseInt(input) || 0);
      document.querySelectorAll(".bai-tap-2 .txt-result-input")[0].innerHTML +=
        input + " ";
    });
  document
    .querySelectorAll(".btn-calc-bai-2")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_2(bai2Input);
    });

  // giai bai 3
  let input = "";
  document
    .querySelectorAll(".bai-tap-3 #btn-add")[0]
    .addEventListener("click", () => {
      input =
        parseInt(
          document.querySelectorAll(".bai-tap-3 #inputNumber")[0].value
        ) || 0;
      document.querySelectorAll(".bai-tap-3 .txt-result-input")[0].innerHTML =
        input;
    });
  document
    .querySelectorAll(".btn-calc-bai-3")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_3(input);
    });

  // giai bai 4
  document
    .querySelectorAll(".btn-calc-bai-4")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_4();
    });

  // giai bai 5
  document
    .querySelectorAll(".btn-calc-bai-5")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_5();
    });

  // giai bai 6
  document
    .querySelectorAll(".btn-calc-bai-6")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_6();
    });

  // giai bai 7
  document
    .querySelectorAll(".btn-calc-bai-7")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_7();
    });

  document
    .querySelectorAll(".btn-calc-bai-8")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_8();
    });

  document
    .querySelectorAll(".btn-calc-bai-9")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_9();
    });

  document
    .querySelectorAll(".btn-calc-bai-10")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_10();
    });
};

main();
